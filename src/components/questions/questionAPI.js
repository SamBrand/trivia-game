export const QuestionAPI = {
    fetchQuestions({numberOfQuest, categories, difficulty, questionType}) { 
        return fetch(`https://opentdb.com/api.php?amount=${numberOfQuest}&category=${categories}&difficulty=${difficulty}&type=${questionType}`)
            .then(response => response.json())
            .then(response => response.results)
    },
    fetchQuestionCategories() {
        return fetch("https://opentdb.com/api_category.php")
            .then(response => response.json())
    }
}



/*export function fetchQuestions(input) {
    return fetch(`https://opentdb.com/api.php?amount=${input.questions_num}&category=${input.category_value}&difficulty=${input.difficulty_value}&type=${input.type_value}`)
        .then(response => response.json())
        .then(response => response.results)
}
export function fetchCategories(input) {
    return fetch(`https://opentdb.com/api_category.php`)
        .then(response => response.json())
}*/