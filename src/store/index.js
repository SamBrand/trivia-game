import Vue from 'vue'
import Vuex from 'vuex'
import { QuestionAPI } from '@/components/questions/questionAPI'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        category: '',
        number_of_questions: 0,
        questiontype: '',
        questions: [],
        question_id: '',
        correctAnswer_nr: '',
        userAnswer: [],
        difficulty: [],
        points: 0,
        error: ''
    },
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setCategory: (state, payload) => {
            state.category = payload
        },
        setNumberOfQuestions: (state, payload) => {
            state.number_of_questions = payload
        },
        setQuestionType: (state, payload) => {
            state.questiontype = payload
        },
        setUserAnswers: (state, payload) => {
            state.userAnswer = payload
        },
        setUserPoints: (state, payload) => {
            if (payload === 0) {
                state.points = 0
            } else {
                state.points += payload // state.points + payload
            }
        },
        setError: (state, error) => {
            state.error = error
        }
    },
    getters: {
        getQuestions: (state) => questionIndex => {
            return state.questions[questionIndex]
        },
        getUserAnswers: (state) => payload => {
            return state.userAnswer[payload]
        },
    },
    actions: {
        async fetchQuestions({ commit }, setup) {
            try {
                const questions = await QuestionAPI.fetchQuestions(setup)
                console.log(questions)
                commit("setQuestions", questions)
                commit("setNumberOfQuestions", questions.length)
            } catch (e) {
                commit("setError", e.message)
            }
        },
    }
})