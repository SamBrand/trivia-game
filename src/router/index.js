import Vue from "vue"
import VueRouter from "vue-router"
import StartPage from "@/components/startpage/StartPage"
//import QuizPage from "@/components/quizpage/QuizPage"
//import ResultPage from "@/components/resultpage/ResultPage"

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'StartPage',
        component: StartPage//() => import(/*StartPage*/ './components/startpage/StartPage.vue')
    },
    {
        path: '/quiz',
        name: 'QuizPage',
        component: () => import('../components/quizpage/QuizPage.vue')
    },
    {
        path: '/result',
        name: 'ResultPage',
        component: () => import('../components/resultpage/ResultPage.vue')
    }
]

export default new VueRouter({ routes });
